# ROCKCOTE user

User data structures and functionality.

## Installation

Add the following lines to `repositories` section of `composer.json`

```
  ...
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_user.git"
    }
  ],
  ...
  "extra": {
    "installer-paths": {
      ...
      "web/modules/custom/{$name}": [
        "type:drupal-module-custom"
      ],
      ...
    }
  }
  ...
```

Run composer command

```
  composer require rockcote/rockcote_user
```

Additionally, you can exclude current module in `.gitignore`

```
/web/modules/custom/rockcote_user
```

## Updating

To update to the latest version run

```
composer update rockcote/rockcote_user --with-dependencies
```

## Issues

Move to [bitbucket](https://bitbucket.org/rockcote/rockcote_user) from [gitlab](https://gitlab.com/rockcote-aus/rockcote_user) due to the [composer issue](https://github.com/composer/composer/issues/6642) and [gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/37355).


## Repositories

* [Bitbucket](https://bitbucket.org/rockcote/rockcote_user): active
* [GitLab](https://gitlab.com/rockcote-aus/rockcote_user): composer prevents from using GitLab as of 04 Sep 2017. See [issues](#issues).
